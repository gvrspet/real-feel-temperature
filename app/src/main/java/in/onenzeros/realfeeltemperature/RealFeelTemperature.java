package in.onenzeros.realfeeltemperature;

public class RealFeelTemperature {
    private double humidity;
    private double windSpeed;
    private double temperature;

    public RealFeelTemperature(double humidity, double windSpeed, double temperature) {
        this.humidity = humidity;
        this.windSpeed = windSpeed;
        this.temperature = temperature;
    }


    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public void setHumidity(double humidity) {
        this.humidity = humidity;
    }


    public void setWindSpeed(double windSpeed) {
        this.windSpeed = windSpeed;
    }

    public double computeRealFeelTemperature() {
        return temperature + (0.348 * computeWaterVapourPressure()) - (0.70 * windSpeed) - 4.25;
    }

    private double computeWaterVapourPressure() {
        return (humidity / 100) * 6.105 * Math.exp(((17.27 * temperature) / (237.7 + temperature)));
    }

}
