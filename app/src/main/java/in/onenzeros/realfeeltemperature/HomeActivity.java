package in.onenzeros.realfeeltemperature;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import in.onenzeros.realfeeltemperature.R;

import java.util.ArrayList;

public class HomeActivity extends AppCompatActivity {

    private AppCompatTextView tvResult;
    private AppCompatEditText etWindSpeed;
    private AppCompatEditText etHumidity;
    private AppCompatEditText etTemperature;
    private AppCompatButton btnCalculate;
    private Spinner spinnerTemp;
    private Spinner spinnerWind;
    private Spinner spinnerHumidity;
    private ArrayList<String> tempUnits = new ArrayList<String>();
    private ArrayList<String> windSpeedUnits = new ArrayList<String>();
    private ArrayList<String> humidityUnit = new ArrayList<String>();
    private ArrayAdapter<String> spinnerTempAdapter;
    private ArrayAdapter<String> spinnerWindAdapter;
    private ArrayAdapter<String> spinnerHumidityAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        initComponents();
    }

    private void initComponents() {
        spinnerTemp = findViewById(R.id.spinnerTemp);
        spinnerWind = findViewById(R.id.spinnerWind);
        spinnerHumidity = findViewById(R.id.spinnerHumidity);
        tempUnits.add((char) 0x00B0 + "C");
        tempUnits.add((char) 0x00B0 + "F");
        windSpeedUnits.add("m/s");
        windSpeedUnits.add("kmph");
        humidityUnit.add("%");
        spinnerTemp.setAdapter(new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, tempUnits));
        spinnerWind.setAdapter(new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, windSpeedUnits));
        spinnerHumidity.setAdapter(new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, humidityUnit));
        etWindSpeed = findViewById(R.id.etWindSpeed);

        etHumidity = findViewById(R.id.etHumidity);
        etTemperature = findViewById(R.id.etTemperature);
        tvResult = findViewById(R.id.tvResult);
        btnCalculate = findViewById(R.id.btnCalculate);
        btnCalculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double temperature;
                double windSpeed;
                double humidity;
                if (isValid()) {
                    try {
                        humidity = Double.parseDouble(etHumidity.getText().toString());
                        windSpeed = Double.parseDouble(etWindSpeed.getText().toString());
                        temperature = Double.parseDouble(etTemperature.getText().toString());
                        if (spinnerTemp.getSelectedItem().toString().equals((char) 0x00B0 + "F"))
                            temperature = UnitConverter.toCelsius(temperature);
                        if (spinnerWind.getSelectedItem().toString().equals("kmph"))
                            windSpeed = UnitConverter.kmphToMps(windSpeed);
                        RealFeelTemperature realFeelTemperature = new RealFeelTemperature(humidity, windSpeed, temperature);
                        tvResult.setText(String.format("%.2f", realFeelTemperature.computeRealFeelTemperature()) + (char) 0x00B0 + "C");
                    } catch (NumberFormatException e) {

                    } catch (Exception e) {

                    }

                }
            }
        });
    }

    private boolean isValid() {
        if (etTemperature.getText().toString().equals("") || etWindSpeed.getText().toString().equals("") || etHumidity.getText().toString().equals("")) {
            Toast.makeText(getApplicationContext(), "", Toast.LENGTH_LONG).show();
            return false;
        } else
            return true;
    }
}